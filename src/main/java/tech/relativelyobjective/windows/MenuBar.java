package tech.relativelyobjective.windows;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;
import tech.relativelyobjective.TextToSpeechMain;

/**
 *
 * @author crussell
 */
public class MenuBar extends JMenuBar {
    private final FrameMain parentWindow;
    FileMenu fileMenu = new FileMenu();
    public MenuBar(FrameMain main) {
        parentWindow = main;
        super.add(fileMenu);
    }
    private class FileMenu extends JMenu {
        private JCheckBoxMenuItem playToFile;
        private JCheckBoxMenuItem playToAudio;
        
        public FileMenu() {
            super("File");
            JMenuItem clear = new JMenuItem("Clear List");
            clear.addActionListener((ActionEvent e)->{
                parentWindow.setText("");
            });
            super.add(clear);
            JMenuItem load = new JMenuItem("Load .csv List");
            load.addActionListener((ActionEvent e)->{
                chooseAndLoadFile();
            });
            super.add(load);
            super.addSeparator();
            playToFile = new JCheckBoxMenuItem("Save to File");
            playToFile.addActionListener((ActionEvent e)->{
                TextToSpeechMain.processMode = TextToSpeechMain.Mode.FILE;
                playToFile.setSelected(true);
                playToAudio.setSelected(false);
            });
            super.add(playToFile);
            playToAudio = new JCheckBoxMenuItem("Play Audio");
            playToAudio.setSelected(true); //Default value
            playToAudio.addActionListener((ActionEvent e)->{
                TextToSpeechMain.processMode = TextToSpeechMain.Mode.AUDIO;
                playToFile.setSelected(false);
                playToAudio.setSelected(true);
            });
            super.add(playToAudio);
        }
    }
    private void chooseAndLoadFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma-Separated Values", "csv");
        fileChooser.setFileFilter(filter);
        if (fileChooser.showOpenDialog(parentWindow) == JFileChooser.APPROVE_OPTION) {
            File savedLocation = fileChooser.getSelectedFile();
            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(savedLocation));
                String line = reader.readLine();
                String textToAdd = "";
                while (line != null) {
                    System.out.println(line);
                    textToAdd += line;
                    textToAdd += "\n";
                    line = reader.readLine();
                }
                parentWindow.setText(textToAdd);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
}
