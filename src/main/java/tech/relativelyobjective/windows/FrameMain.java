package tech.relativelyobjective.windows;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;
import marytts.signalproc.effects.BaseAudioEffect;
import marytts.signalproc.effects.HMMF0ScaleEffect;
import marytts.signalproc.effects.VocalTractLinearScalerEffect;
import tech.relativelyobjective.TextToSpeechMain;
import tech.relativelyobjective.utilities.TextToSpeechEngine;

/**
 *
 * @author crussell
 */
public class FrameMain extends JFrame {
    private JTextArea textArea = new JTextArea();
    
    public FrameMain() {
        super("TextToSpeech by Christian Russell");
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setPreferredSize(new Dimension(850, 580));
        super.setSize(super.getPreferredSize());
        //Menu
        super.setJMenuBar(new MenuBar(this));
        //Window Contents
        super.setLayout(new BorderLayout());
        JPanel textBorderedArea = new JPanel();
        textBorderedArea.setBorder(BorderFactory.createTitledBorder("Text To Process (Each Line is a Different File)"));
        textBorderedArea.setLayout(new BorderLayout());
        textArea.setText("extensionNumber,FirstName,LastName");
        textBorderedArea.add(textArea,BorderLayout.CENTER);
        super.add(textBorderedArea, BorderLayout.CENTER);
        JButton processButton = new JButton("Process");
        processButton.addActionListener((ActionEvent e)->{
            processTextMary();
        });
        super.add(processButton, BorderLayout.SOUTH);
    }
    public void processTextGoogle() {
    
    }
    public void processTextMary() {
        if (TextToSpeechMain.processMode == TextToSpeechMain.Mode.FILE) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(TextToSpeechMain.workingDirectory);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File savedLocation = fileChooser.getSelectedFile();
                if (savedLocation.isFile()) {
                    savedLocation = savedLocation.getParentFile();
                }
                TextToSpeechMain.workingDirectory = savedLocation;
            } else {
                return;
            }
        }
        
        //Create TextToSpeech
        TextToSpeechEngine tts = new TextToSpeechEngine();

        // Setting the Current Voice
        tts.setVoice("cmu-rms-hsmm");

        //Create spot to hold voice effects
        LinkedList<BaseAudioEffect> voiceEffects = new LinkedList<>();

        //HMMF0ScaleEffect (natural > smoothing)
        HMMF0ScaleEffect scaleEffect = new HMMF0ScaleEffect();
        scaleEffect.setParams("amount:3");
        voiceEffects.add(scaleEffect);

        //HMMDurationScaleEffect (speed)
        VocalTractLinearScalerEffect speedEffect = new VocalTractLinearScalerEffect();
        speedEffect.setParams("amount:1.0");
        voiceEffects.add(speedEffect);

        //Apply voice effects
        boolean first = true;
        String effects = "";
        for (BaseAudioEffect eff : voiceEffects) {
            if (!first) {
                effects += "+";
            } else {
                first = false;
            }
            effects += eff.getFullEffectAsString();
        }
        System.out.printf("Audio Effects: %s\n", effects);
        tts.getMarytts().setAudioEffects(effects);

        //Loop through list
        //arrayList.forEach(word -> tts.speak(word, 2.0f, false, true));
        for (String lineText : getTextObjects()) {
            String[] lineValues = lineText.split(",");
            if (lineValues.length != 3) {
                System.out.printf("String does not match convention ext#,fname,lname: %s\n", lineText);
                continue;
            }
            int extNumber;
            try {
                extNumber = Integer.parseInt(lineValues[0]);
            } catch (NumberFormatException e) {
                System.out.printf("Cannot parse extension number: %s\n", lineValues[0]);
                continue;
            }
            String name = String.format("%s %s", lineValues[1], lineValues[2]);
            if (TextToSpeechMain.processMode == TextToSpeechMain.Mode.AUDIO) {
                System.out.printf("Speaking ext. %d (%s)\n", extNumber, name);
                tts.speak(name, 2.0f, false, true);
            } else {
                File saveLocation = new File(
                    TextToSpeechMain.workingDirectory.getAbsolutePath()+File.separatorChar+
                    "data"+File.separatorChar+extNumber+File.separatorChar+
                    "self_id_prompt.wav"
                );
                saveLocation.getParentFile().mkdirs();
                System.out.printf("Saving ext. %d to file (%s): \"%s\"\n", extNumber, name, saveLocation.getAbsoluteFile());
                tts.speakToFile(name, saveLocation);
            }
        }
    }
    public final List<String> getTextObjects() {
        return Arrays.asList(textArea.getText().split("\n"));
    }
    public void setText(String s) {
        textArea.setText(s);
    }
}
