package tech.relativelyobjective;

import java.io.File;
import tech.relativelyobjective.windows.FrameMain;

/*
 *
 * @author crussell
 */
public class TextToSpeechMain {
    public static File workingDirectory = new File(".");
    public static Mode processMode = Mode.AUDIO;
    
    public enum Mode {
        FILE,
        AUDIO
    }
    
    public static void main(String[] args) {
        FrameMain mainFrame = new FrameMain();
        mainFrame.setVisible(true);
    }
}
